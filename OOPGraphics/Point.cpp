#include "Point.h"


void Point::draw()
{
	window->draw(point);

}
bool Point::checkPosition(double x, double y)
{
	int pX = point.getPosition().x, pY = point.getPosition().y;
	return pow(pX + R - x, 2) + pow(pY + R - y, 2) <= 4 * R * R;
}
Point::Point(sf::RenderWindow *window, double x, double y)
{
	this->window = window;
	this->x = x;
	this->y = y;
	point = sf::CircleShape(R);
	point.setPosition(sf::Vector2f(x - R, y - R));
}

Point::Point(sf::RenderWindow* window, double x, double y, sf::Color color) : Point(window,x,y)
{
	setColor(color);
}

double Point::getX()
{
	return x;
}
double Point::getY()
{
	return y;
}

void Point::setX(double x)
{
	this->x = x;
}

void Point::setY(double y)
{
	this->y = y;
}

void Point::setColor(sf::Color color)
{
	point.setFillColor(color);
}
bool Point::operator ==(const Point& other) const
{
	return this->x == other.x && this->y == other.y;
}