#pragma once
#include "Builder.h"
#include "Functions.h"

class ConvexShellBuilder : public Builder
{
private:
	bool isSorted;
	bool upsideCompleted;
	std::vector<bool> perms;
	std::vector<std::pair<Point, int>> upSide;
	std::vector<std::pair<Point, int>> downSide;
	double k, b;
	Point left;
	Point right;
	int index;
	int answerSideIndex;
	sf::RenderWindow* window;
public:
	std::vector<std::pair<Point, int>> answer;
	std::vector<int> originalIndexes;
	void step();
	void start(std::vector<Point> points);
	void start(std::vector<Point> points, std::vector<bool> perms);
	void restartWithPermissions(std::vector<bool> perms);
	void stop();
	ConvexShellBuilder(sf::RenderWindow* window);
};

