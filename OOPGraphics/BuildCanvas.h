#pragma once
#include "IClickable.h"
#include "IDrawable.h"
#include "Point.h"
#include <vector>

class BuildCanvas : public IClickable, public IDrawable
{
private:
	sf::RectangleShape body;
	int pointsAmount;
	std::vector<Point> points;
public:

	int getPointsAmount();
	std::vector<Point> getPoints();
	void draw();
	bool processClick(int x, int y);
	void drawPoints();
	void clear();
	BuildCanvas(sf::RenderWindow* window, int x, int y, int w, int h);
};

