#pragma once
#include <SFML/Graphics.hpp>

class IDrawable
{
protected:
	sf::RenderWindow *window;
public:
	virtual void draw() = 0;
};

