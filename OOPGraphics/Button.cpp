#include "Button.h"


void Button::draw()
{

	window->draw(body);
	window->draw(text);


	if (clickedCounter == 0) {
		body.setOutlineColor(sf::Color(200, 200, 200));
	}
	if (clickedCounter > 0)
		clickedCounter--;
}

bool Button::processClick(int x, int y) {
	int bodyX = body.getPosition().x, bodyY = body.getPosition().y;
	int bodyWidth = body.getSize().x, bodyHeight = body.getSize().y;
	if (x < bodyX || x > bodyX + bodyWidth|| y < bodyY || y > bodyY + bodyHeight)
		return true;

	clickedCounter = 50;
	body.setOutlineColor(sf::Color(150, 150, 150));

	onClick();
	return true;
}
Button::Button(sf::RenderWindow *window, std::string text, int x, int y, int w, int h, void onClickHandler())
{
	clickedCounter = 0;
	this->onClick = onClickHandler;
	this->window = window;
	body = sf::RectangleShape(sf::Vector2f(w, h));
	body.setPosition(sf::Vector2f(x, y));
	body.setFillColor(sf::Color(100, 100, 100));
	body.setOutlineColor(sf::Color(200, 200, 200));
	body.setOutlineThickness(-5);
	font.loadFromFile("arial.ttf");
	this->text = sf::Text(text, font, 20);
	this->text.setFillColor(sf::Color(250, 250, 250));
	this->text.setPosition(x + w * 0.125, y + h * 0.4);

}
