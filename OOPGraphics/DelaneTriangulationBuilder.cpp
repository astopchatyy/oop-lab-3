#include "DelaneTriangulationBuilder.h"




bool cmpPoints(Point a, Point b)
{
	return a.getX() == b.getX() && a.getY() == b.getY();
}
void DelaneTriangulationBuilder::step()
{
	std::vector<bool> trianglesValidator;
	std::vector<std::pair<int, int>> edges;
	int edgeCounter[200][200] = { 0 };
	if (index == 0)
	{

		index = 3;
		Triangle t = { points[0],points[1],points[2],0,1,2 };
		points[0].setColor(sf::Color::Yellow);
		points[1].setColor(sf::Color::Yellow);
		points[2].setColor(sf::Color::Yellow);
		addTriangle(t);
	}
	else if (index < pointsSize)
	{

		points[index].setColor(sf::Color::Yellow);
		
		for (int i = 0; i < triangles.size(); i++)
		{
			Triangle t = triangles[i];
			if (isPointInsideTriangleCircle(t, points[index]))
			{
				trianglesValidator.push_back(false);
				edges.push_back({ t.aPointIndex, t.bPointIndex });
				edgeCounter[t.aPointIndex][t.bPointIndex]++;
				edgeCounter[t.bPointIndex][t.aPointIndex]++;
				edges.push_back({ t.aPointIndex, t.cPointIndex });
				edgeCounter[t.aPointIndex][t.cPointIndex]++;
				edgeCounter[t.cPointIndex][t.aPointIndex]++;
				edges.push_back({ t.cPointIndex, t.bPointIndex });
				edgeCounter[t.cPointIndex][t.bPointIndex]++;
				edgeCounter[t.bPointIndex][t.cPointIndex]++;
			}
			else
				trianglesValidator.push_back(true);
		}
		std::pair<int, int> edge;
		for (int i = 0; i < edges.size(); i++)
		{
			edge = edges[i];
			if (edgeCounter[edge.first][edge.second] == 1)
			{
				triangles.push_back({ points[edge.first], points[edge.second], points[index], edge.first, edge.second, index });
				trianglesValidator.push_back(true);
			}
		}
		oldTriangles = triangles;
		triangles.clear();

		lines.clear();
		for (int i = 0; i < oldTriangles.size(); i++)
		{
			if (trianglesValidator[i])
			{
				triangles.push_back(oldTriangles[i]);
				if(dist(oldTriangles[i].a, oldTriangles[i].b) < 1000)
					lines.push_back(Line(window, oldTriangles[i].a, oldTriangles[i].b, sf::Color(45, 165, 45)));

				if (dist(oldTriangles[i].c, oldTriangles[i].b) < 1000)
					lines.push_back(Line(window, oldTriangles[i].c, oldTriangles[i].b, sf::Color(45, 165, 45)));

				if (dist(oldTriangles[i].a, oldTriangles[i].c) < 1000)
					lines.push_back(Line(window, oldTriangles[i].a, oldTriangles[i].c, sf::Color(45, 165, 45)));
			}
		}
		index++;

	}
	else if (index >= pointsSize)
	{
		isStarted = false;
		points.clear();
		shellBuilder->start(originalPoints);
		while (shellBuilder->isStarted)
		{
			shellBuilder->step();
		}
		for (int i = 1; i < shellBuilder->answer.size(); i++)
		{
			lines.push_back(Line(window, shellBuilder->answer[i].first, shellBuilder->answer[i-1].first, sf::Color(45, 165, 45)));
		}
	}
}

bool DelaneTriangulationBuilder::isPointInsideTriangleCircle(Triangle t, Point p)
{
	Point center = getCircle(t);
	return dist(center, p) + 1e-8 <= dist(center, t.a);
}
bool DelaneTriangulationBuilder::isPointInsideTriangle(Triangle t, Point p)
{
	bool checker = true;
	checker = checker && !isPointInsideTriangleCircle({ t.a, t.b, points[index], t.aPointIndex, t.bPointIndex, index }, t.c);
	checker = checker && !isPointInsideTriangleCircle({ t.a, t.c, points[index], t.aPointIndex, t.cPointIndex, index }, t.b);
	checker = checker && !isPointInsideTriangleCircle({ t.c, t.b, points[index], t.cPointIndex, t.bPointIndex, index }, t.a);
	return checker;
}
bool DelaneTriangulationBuilder::tryAddTriangleWithValidation(Triangle t, Point p)
{
	if (!isPointInsideTriangleCircle(t,p))
	{
		addTriangle(t);
		return true;
	}
	return false;
}

void DelaneTriangulationBuilder::addTriangle(Triangle t)
{
	triangles.push_back(t);
	oldTriangles.push_back(t);
}

void DelaneTriangulationBuilder::deleteTriangle(Triangle t)
{
	triangles.erase(std::find(triangles.begin(), triangles.end(), t));
}
void DelaneTriangulationBuilder::start(std::vector<Point> points)
{
	isStarted = true;
	index = 0;

	originalPoints = points;
	points.insert(points.begin(), Point(window, 2000, 600));
	points.insert(points.begin(), Point(window, 300, -2000));
	points.insert(points.begin(), Point(window, 300, 2000));
	pointsSize = points.size();
	for (int i = 0; i < points.size(); i++)
	{
		Point p1 = Point(window, points[i].getX(), points[i].getY());
		this->points.push_back(p1);
	}
	triangles.clear();
	lines.clear();
	step();
	step();
	step();
}
void DelaneTriangulationBuilder::stop()
{
	isStarted = false;
	triangles.clear();
	points.clear();
	lines.clear();
}

DelaneTriangulationBuilder::DelaneTriangulationBuilder(sf::RenderWindow* window)
{
	isStarted = false;
	this->window = window;
	points = std::vector<Point>();
	triangles = std::vector<Triangle>();
	oldTriangles = std::vector<Triangle>();
	lines = std::vector<Line>();
	shellBuilder = new ConvexShellBuilder(window);
}