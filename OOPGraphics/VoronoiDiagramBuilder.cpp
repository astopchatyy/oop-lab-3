#include "VoronoiDiagramBuilder.h"
#include <set>




void VoronoiDiagramBuilder::step()
{
	if (triangulationBuilder->isStarted)
	{
		triangulationBuilder->step();
		std::vector<Triangle> triangles = triangulationBuilder->triangles;
		points.clear();
		lines.clear();
		Point c1, c2;
		Triangle t1, t2;
		double k, b;
		int cnt;
		for (int i = 0; i < triangles.size(); i++)
		{
			for (int j = i + 1; j < triangles.size(); j++)
			{
				t1 = triangles[i];
				t2 = triangles[j];
				cnt = 0;
				if (t1.aPointIndex == t2.aPointIndex || t1.aPointIndex == t2.bPointIndex || t1.aPointIndex == t2.cPointIndex)
				{
					cnt++;
					points.push_back(Point(window, t1.a.getX(), t1.a.getY(), sf::Color::Yellow));
				}
				if (t1.bPointIndex == t2.aPointIndex || t1.bPointIndex == t2.bPointIndex || t1.bPointIndex == t2.cPointIndex)
				{
					cnt++;
					points.push_back(Point(window, t1.b.getX(), t1.b.getY(), sf::Color::Yellow));
				}
				if (t1.cPointIndex == t2.aPointIndex || t1.cPointIndex == t2.bPointIndex || t1.cPointIndex == t2.cPointIndex)
				{
					cnt++;
					points.push_back(Point(window, t1.c.getX(), t1.c.getY(), sf::Color::Yellow));
				}
				if (cnt == 2)
				{
					c1 = getCircle(t1, window);

					c2 = getCircle(t2, window);

					if (c1.getX() < 300)
					{
						std::swap(c1, c2);
					}

					if (c2.getX() < 300)
					{
						k = (c1.getY() - c2.getY()) / (c1.getX() - c2.getX());
						b = c2.getY() - c2.getX() * k;
						c2.setX(300);
						c2.setY(300 * k + b);
					}
					if(c1.getX() > 300)
					lines.push_back(Line(window, c1, c2, sf::Color(45, 45, 125)));
				}
				else if (cnt == 1)
					points.pop_back();
			}
		}
	}
	else
	{
		isStarted = false;
		points.clear();
	}
}
void VoronoiDiagramBuilder::start(std::vector<Point> points)
{
	index = 0;
	isStarted = true;
	pointsSize = points.size();
	triangulationBuilder->start(points);
}
void VoronoiDiagramBuilder::stop()
{
	points.clear();
	lines.clear();
	isStarted = false;
}
VoronoiDiagramBuilder::VoronoiDiagramBuilder(sf::RenderWindow* window)
{
	triangulationBuilder = new DelaneTriangulationBuilder(window);
	isStarted = false;
	this->window = window;
	lines = std::vector<Line>();
	points = std::vector<Point>();
}