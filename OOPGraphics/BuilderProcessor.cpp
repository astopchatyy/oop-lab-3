#include "BuilderProcessor.h"

void BuilderProcessor::draw()
{
	if (isInitialized)
	{
		if (builder->isStarted)
		{
			if (timer == 0)
			{
				builder->step();
				timer = 151;
			}
			timer--;
		}
		for (int i = 0; i < builder->lines.size(); i++)
		{
			builder->lines[i].draw();
		}
		for (int i = 0; i < builder->points.size(); i++)
		{
			builder->points[i].draw();
		}
	}
}

BuilderProcessor::BuilderProcessor() : isInitialized(false) {}

void BuilderProcessor::setBuilder(Builder* builder)
{
	timer = 0;
	this->builder = builder;
	isInitialized = true;
}

void BuilderProcessor::stop()
{
	if (!isInitialized)
		return;
	builder->stop();
}

void BuilderProcessor::start(std::vector<Point> points )
{
	if (!isInitialized)
		return;
	builder->start(points);
}