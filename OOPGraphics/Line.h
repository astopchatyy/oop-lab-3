#pragma once
#include "IDrawable.h"
#include "Point.h"
#include <SFML/Graphics.hpp>

class Line : public IDrawable
{
private:

	sf::Vertex line[2];

	double x1, y1, x2, y2;
	Point a;
	Point b;

public:
	void draw();
	Line(sf::RenderWindow* window, Line line);
	Line(sf::RenderWindow* window, Point a, Point b, sf::Color color = sf::Color(255,255,255));
	void setColor(sf::Color color);
	Line() {}
};

