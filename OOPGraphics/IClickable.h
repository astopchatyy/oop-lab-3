#pragma once
class IClickable
{
public:
	virtual bool processClick(int x, int y) = 0;
};

