#pragma once
#include "IDrawable.h"
#include "IClickable.h"
#include <SFML/Graphics.hpp>

class Button : public IDrawable, public IClickable
{
private:
	void (*onClick)();
	sf::RectangleShape body;
	sf::Text text;
	sf::Font font;
	int clickedCounter;
public:
	void draw();
	bool processClick(int x, int y);
	Button(sf::RenderWindow* window, std::string text, int x, int y, int w, int h, void onClickHandler());
};