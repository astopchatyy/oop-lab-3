#pragma once
#include "Point.h"

struct Triangle
{
	Point a;
	Point b;
	Point c;
	int aPointIndex;
	int bPointIndex;
	int cPointIndex;
	bool operator == (Triangle other)
	{
		return this->a == other.a && this->b == other.b && this->c == other.c;
	}

};

