#pragma once
#include "Builder.h"
#include "Functions.h"
#include "Line.h"
#include "ConvexShellBuilder.h"
#include "Triangle.h"

class DelaneTriangulationBuilder : public Builder
{
private:
	sf::RenderWindow* window;
	std::vector<Triangle> oldTriangles;
	int index = 0;
	int pointsSize;
	void addTriangle(Triangle t);
	void deleteTriangle(Triangle t);
	bool tryAddTriangleWithValidation(Triangle t, Point p);
	bool isPointInsideTriangleCircle(Triangle t, Point p);
	bool isPointInsideTriangle(Triangle t, Point p);
public:
	ConvexShellBuilder* shellBuilder;
	std::vector<Triangle> triangles;
	void step();
	void start(std::vector<Point> points);
	void stop();
	DelaneTriangulationBuilder(sf::RenderWindow* window);
};

