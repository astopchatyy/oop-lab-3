#pragma once
#include "Builder.h"
#include "Functions.h"
#include "DelaneTriangulationBuilder.h"
#include "Triangle.h"


class VoronoiDiagramBuilder : public Builder
{
private:
	int index = 0;
	sf::RenderWindow* window;
	int pointsSize;
	DelaneTriangulationBuilder* triangulationBuilder;
public:
	void step();
	void start(std::vector<Point> points);
	void stop();
	VoronoiDiagramBuilder(sf::RenderWindow* window);
};

