#pragma once
#include "Builder.h"
#include "IDrawable.h"

class BuilderProcessor : public IDrawable
{
private:
	bool isInitialized;
	int timer;
	Builder* builder;
public:
	void stop();
	void start(std::vector<Point> points);
	void setBuilder(Builder* builder);
	void draw();
	BuilderProcessor();
};

