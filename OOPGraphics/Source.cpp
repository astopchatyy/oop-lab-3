#include <SFML/Graphics.hpp>
#include <vector>
#include "Functions.h"
#include "IDrawable.h"
#include "IClickable.h"
#include "BuildCanvas.h"
#include "Button.h"
#include "BuilderProcessor.h"
#include "ConvexShellBuilder.h"
#include "VoronoiDiagramBuilder.h"
#include "DelaneTriangulationBuilder.h"
#include <fstream>
#include <random>
#include<time.h>

int buttonWidth = 300;
int canvasWidth = 800;
int width = buttonWidth + canvasWidth;
int height = 800;
sf::RenderWindow window(sf::VideoMode(width, height), "OOP Lab 3");
BuildCanvas canvas(&window, buttonWidth,0, canvasWidth, height);

BuilderProcessor processor;

ConvexShellBuilder* shellBuilder;
VoronoiDiagramBuilder* diagramBuilder;
DelaneTriangulationBuilder* triangulationBuilder;

void loadFromFile()
{

	processor.stop();
	canvas.clear();
	std::ifstream stream("input.txt");
	int n, x, y;
	stream >> n;
	for (int i = 0; i < n; i++)
	{
		stream >> x >> y;
		canvas.processClick(x + buttonWidth, y);
	}
}
void generateRandom()
{

	processor.stop();
	canvas.clear();
	int n, x, y;
	n = getRandBetween(10, 25);

	for (int i = 0; i < n; i++)
	{
		x = getRandBetween(buttonWidth + 100, width - 100);
		y = getRandBetween(100, height - 100);
		canvas.processClick(x, y);
	}
}

void buildShell()
{
	if (canvas.getPointsAmount() > 2)
	{
		processor.stop();
		processor.setBuilder(shellBuilder);
		processor.start(canvas.getPoints());
	}
}
void buildDiagram()
{
	if (canvas.getPointsAmount() > 2)
	{
		processor.stop();
		processor.setBuilder(diagramBuilder);
		processor.start(canvas.getPoints());
	}
}
void buildTriangulation()
{
	if (canvas.getPointsAmount() > 2)
	{
		processor.stop();
		processor.setBuilder(triangulationBuilder);
		processor.start(canvas.getPoints());
	}
}

void clear()
{
	processor.stop();
	canvas.clear();
}
int main()
{
	srand(time(0));

	shellBuilder = new ConvexShellBuilder(&window);
	diagramBuilder = new VoronoiDiagramBuilder(&window);
	triangulationBuilder = new DelaneTriangulationBuilder(&window);

	std::vector<IDrawable*> drawableWindowElements;
	std::vector<IClickable*> clickableWindowElements;

	Button loadFromFileButton(&window, "Load from file", 0, 0, buttonWidth, 100, loadFromFile);
	Button generateRandomButton(&window, "Generate random", 0, 100, buttonWidth, 100, generateRandom);
	Button buildShellButton(&window, "Build convex shell", 0, 300, buttonWidth, 100, buildShell);
	Button buildDiagramButton(&window, "Build Voronoi diagram", 0, 400, buttonWidth, 100, buildDiagram);
	Button buildTriangulationButton(&window, "Build Delone triangulation", 0, 500, buttonWidth, 100, buildTriangulation);

	Button clearButton(&window, "Clear", 0, 700, buttonWidth, 100, clear);

	drawableWindowElements.push_back(&loadFromFileButton);
	clickableWindowElements.push_back(&loadFromFileButton);

	drawableWindowElements.push_back(&generateRandomButton);
	clickableWindowElements.push_back(&generateRandomButton);

	drawableWindowElements.push_back(&buildShellButton);
	clickableWindowElements.push_back(&buildShellButton);

	drawableWindowElements.push_back(&buildDiagramButton);
	clickableWindowElements.push_back(&buildDiagramButton);

	drawableWindowElements.push_back(&buildTriangulationButton);
	clickableWindowElements.push_back(&buildTriangulationButton);

	drawableWindowElements.push_back(&clearButton);
	clickableWindowElements.push_back(&clearButton);

	drawableWindowElements.push_back(&canvas);
	clickableWindowElements.push_back(&canvas);

	processor = BuilderProcessor();
	drawableWindowElements.push_back(&processor);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::EventType::MouseButtonPressed)
			{
				int x, y;
				x = event.mouseButton.x;
				y = event.mouseButton.y;
				for (int i = 0; i < clickableWindowElements.size(); i++)
				{
					clickableWindowElements[i]->processClick(x, y);
				}
			}
			else if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();

		for (int i = 0; i < drawableWindowElements.size(); i++)
		{
			drawableWindowElements[i]->draw();
		}

		window.display();

	}

	return 0;
}