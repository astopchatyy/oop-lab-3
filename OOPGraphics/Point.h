#pragma once
#include "IDrawable.h"
#include <SFML/Graphics.hpp>

class Point : public IDrawable
{
private:
	sf::CircleShape point;
	double x;
	double y;
public:
	double getX();
	double getY();
	void setX(double x);
	void setY(double y);
	static const int R = 5;
	void draw();
	bool checkPosition(double x, double y);
	Point() {}
	Point(sf::RenderWindow* window, double x, double y);
	Point(sf::RenderWindow* window, double x, double y, sf::Color color);
	void setColor(sf::Color color);
	bool operator ==(const Point& other) const;
};

