#pragma once
#include "Point.h"
#include "Line.h"
#include "Triangle.h"


int getRandBetween(int a, int b);

Point getCircle(Point a, Point b, Point c, sf::RenderWindow* window = NULL);
Point getCircle(Triangle t, sf::RenderWindow* window = NULL);

double dist(Point a, Point b);
