#include "Line.h"


void Line::draw()
{
	window->draw(line, 2, sf::Lines);
}
Line::Line(sf::RenderWindow* window, Line line) : Line(window, line.a, line.b)
{

}
Line::Line(sf::RenderWindow* window, Point a, Point b, sf::Color color)
{
	this->window = window;

	this->a = a;
	this->b = b;

	x1 = a.getX();
	y1 = a.getY();
	x2 = b.getX();
	y2 = b.getY();

	line[0] = sf::Vertex(sf::Vector2f(x1, y1));
	line[1] = sf::Vertex(sf::Vector2f(x2, y2));

	setColor(color);
}

void Line::setColor(sf::Color color)
{
	line[0].color = color;
	line[1].color = color;
}