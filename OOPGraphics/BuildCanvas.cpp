#include "BuildCanvas.h"


void BuildCanvas::draw()
{
	window->draw(body);
	drawPoints();
}

bool BuildCanvas::processClick(int x, int y)
{
	int bodyX = body.getPosition().x, bodyY = body.getPosition().y;
	int bodyWidth = body.getSize().x, bodyHeight = body.getSize().y;
	if (x < bodyX || x > bodyX + bodyWidth || y < bodyY || y > bodyY + bodyHeight)
		return true;
	for (int i = 0; i < points.size(); i++)
	{
		if (points[i].checkPosition(x, y))
		{
			points.erase(points.begin() + i);
			return true;
		}
	}
	points.push_back(Point(window, x, y));
	return true;
}
BuildCanvas::BuildCanvas(sf::RenderWindow* window, int x, int y, int w, int h)
{
	points = std::vector<Point>();
	this->window = window;
	body = sf::RectangleShape(sf::Vector2f( w,h ));
	body.setPosition(sf::Vector2f(x, y));
	body.setFillColor(sf::Color(50, 50, 50));
}
void BuildCanvas::drawPoints()
{
	for (int i = 0; i < points.size(); i++)
	{
		points[i].draw();
	}
}
void BuildCanvas::clear()
{
	int l = points.size() - 1;
	for (int i = l; i >= 0; i--)
	{
		points.erase(points.begin()+i);
	}
}

int BuildCanvas::getPointsAmount()
{
	return points.size();
}

std::vector<Point> BuildCanvas::getPoints()
{
	return points;
}