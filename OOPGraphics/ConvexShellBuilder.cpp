#include "ConvexShellBuilder.h"

bool compPointsByX(std::pair<Point, int> a, std::pair<Point, int>  b)
{
	if (a.first.getX() == b.first.getX())
		return a.first.getY() > b.first.getY();
	return a.first.getX() < b.first.getX();
}

bool isInternal(Point a, Point b, Point c)
{
	return a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY()) + c.getX() * (a.getY() - b.getY()) < 0;
}

void ConvexShellBuilder::step()
{
	if (!isSorted)
	{
		std::vector<std::pair<Point, int>> v;
		for (int i = 0; i < originalPoints.size(); i++)
		{
			v.push_back({ originalPoints[i],i });
		}
		std::sort(v.begin(), v.end(), compPointsByX);
		originalPoints.clear();
		for (int i = 0; i < v.size(); i++)
		{
			originalPoints.push_back(v[i].first);
			originalIndexes.push_back(v[i].second);
		}
		isSorted = true;
	}
	if(index == 0)
	{
		upsideCompleted = false;
		int i1;
		for (i1 = 0; !perms[originalIndexes[i1]];i1++);
		left = originalPoints[i1];
		int i2;
		for (i2 = originalPoints.size() - 1; !perms[originalIndexes[i2]];i2--);
		right = originalPoints[i2];
		if ((double)(right.getX() - left.getX()) == 0)
		{
			answer.push_back({ left, originalIndexes[i1] });
			answer.push_back({ right, originalIndexes[i2] });

			lines.push_back(Line(window, left, right, sf::Color(165, 45, 45)));
			isStarted = false;
			return;
		}
		k = (double)(right.getY() - left.getY())/ (double)(right.getX() - left.getX());
		b = left.getY() - k * left.getX();
		index = 1;
		answerSideIndex = 0;
		upSide.push_back({ left, originalIndexes[i1] });
		downSide.push_back({ left,originalIndexes[i1] });
		for (int i = i1+1; i < i2; i++)
		{
			if (originalPoints[i].getY() < originalPoints[i].getX() * k + b && perms[originalIndexes[i]])
			{
				upSide.push_back({ originalPoints[i],originalIndexes[i] });
			}
			else if (perms[originalIndexes[i]])
			{
				downSide.push_back({originalPoints[i], originalIndexes[i]});
			}
		}

		upSide.push_back({ right,originalIndexes[i2] });
		downSide.push_back({ right,originalIndexes[i2] });

		std::reverse(downSide.begin(), downSide.end());

		answer.push_back({ left,originalIndexes[i1] });
	}
	if (upsideCompleted)
	{
		while (answerSideIndex > 0 && isInternal(answer[answer.size() - 2].first, answer[answer.size() - 1].first, downSide[index].first))
		{
			answer.pop_back();
			lines.pop_back();
			answerSideIndex--;
		}
		lines.push_back(Line(window, answer[answer.size() - 1].first, downSide[index].first, sf::Color(165, 45, 45)));
		answer.push_back(downSide[index]);
		index++;
		answerSideIndex++;
		if (downSide[index - 1].first == left)
		{
			isStarted = false;
			points.clear();
		}
	}
	else
	{
		while (answerSideIndex > 0 && isInternal(answer[answer.size() - 2].first, answer[answer.size() - 1].first, upSide[index].first))
		{
			answer.pop_back();
			lines.pop_back();
			answerSideIndex--;
		}
		lines.push_back(Line(window, answer[answer.size() - 1].first, upSide[index].first, sf::Color(165, 45, 45)));
		answer.push_back(upSide[index]);
		index++;
		answerSideIndex++;
		if (upSide[index-1].first == right)
		{
			upsideCompleted = true;
			index = 1;
			answerSideIndex = 0;
		}
	}
	if (isStarted)
	{
		points = originalPoints;
		for (int i = 0; i < answer.size(); i++)
		{
			Point p1 = Point(window, answer[i].first.getX(), answer[i].first.getY(), sf::Color::Yellow);
			points.push_back(p1);
		}
	}
}
void ConvexShellBuilder::restartWithPermissions(std::vector<bool> perms)
{
	this->perms = perms;
	index = 0;
	isStarted = true;
	this->points = originalPoints;
	downSide.clear();
	upSide.clear();
	answer.clear();
	while (isStarted)
	{
		step();
	}
}
void ConvexShellBuilder::start(std::vector<Point> points)
{
	start(points, std::vector<bool>());
}
void ConvexShellBuilder::start(std::vector<Point> points, std::vector<bool> perms)
{
	if (perms.size() != points.size())
	{
		this->perms = std::vector<bool>(points.size(), true);
	}
	else
		this->perms = perms;
	index = 0;
	lines.clear();
	downSide.clear();
	upSide.clear();
	answer.clear();
	originalIndexes.clear();
	isStarted = true;
	isSorted = false;
	originalPoints = points;
}
void ConvexShellBuilder::stop()
{
	int l = lines.size() - 1;
	for (int i = l; i >= 0; i--)
	{
		lines.erase(lines.begin() + i);
	}
	points.clear();
	isStarted = false;
}
ConvexShellBuilder::ConvexShellBuilder(sf::RenderWindow* window)
{
	this->window = window;
	lines = std::vector<Line>();
	upSide = std::vector<std::pair<Point, int>>();
	downSide = std::vector<std::pair<Point, int>>();
	answer = std::vector<std::pair<Point, int>>();
	perms = std::vector<bool>();
	originalIndexes= std::vector<int>();
}