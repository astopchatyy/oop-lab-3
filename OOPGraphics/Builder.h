#pragma once
#include <vector>
#include "Point.h"
#include "Line.h"

class Builder
{
protected:
	std::vector<Point> originalPoints;
public:
	bool isStarted;
	std::vector<Point> points;
	std::vector<Line> lines;
	virtual void step() = 0;
	virtual void start(std::vector<Point>) = 0;
	virtual void stop() = 0;
};

