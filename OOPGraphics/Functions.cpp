#include "Functions.h"
#include <random>

int getRandBetween(int a, int b)
{
	return rand() % (b - a + 1) + a;
}

Point getCircle(Triangle t, sf::RenderWindow* window)
{
	return getCircle(t.a, t.b, t.c, window);
}

Point getCircle(Point a, Point b, Point c , sf::RenderWindow* window)
{
	if (b.getX() == a.getX())
		std::swap(b, c);
	if (b.getX() == c.getX())
		std::swap(b, a);
	double t1 = 0;
	if(b.getX() == a.getX())
		t1 = 1e-6;
	double m1 = (b.getY() - a.getY()) / (b.getX() + t1 - a.getX());
	double m2 = (c.getY() - b.getY()) / (c.getX() + t1 - b.getX());



	double x = (m1 * m2 * (a.getY() - c.getY()) + m2 * (b.getX() + a.getX()) - m1 * (c.getX() + b.getX())) / 2 / (m2 - m1);
	double y = -1 / m1 * (x - (b.getX() + a.getX()) / 2) + (b.getY() + a.getY()) / 2;
	return Point(window, x, y);
}

double dist(Point a, Point b)
{
	return sqrt(pow(a.getX() - b.getX(), 2) + pow(a.getY() - b.getY(), 2));
}
